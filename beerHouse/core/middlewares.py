from django.core.exceptions import PermissionDenied

def access_denied_not_admins(get_response):

    def middleware(request):

        if not request.user.is_superuser:
            raise PermissionDenied

        response = get_response(request)

        return response

    return middleware