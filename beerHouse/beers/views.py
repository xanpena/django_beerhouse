from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy
from django.utils.translation import gettext
from django.views import View
from django.views.generic import ListView, DetailView, UpdateView, CreateView
from beers.models import Beer, Company
from beers.mixins import AddHappyBirthdayToContextMixin
from beers.forms import CompanyForm, BeerFormset

# Create your views here.
#@login_required
#def beer_list_view(request):

    #beer_list = Beer.objects.all()
    #beer_list_counter = beer_list.count()
    # beer_list = Beer.objects.filter(pk__in=[])
    # print("beer_list_counter", beer_list_counter)

    # print("exists?", beer_list.filter(id=1))
    # print("exists?", beer_list.filter(id=1).exists())

    # company = Company.objects.create(name="co", tax_number=45678)
    # Beer.objects.create(name="prubea", company=company)

    # company = Company.objects.get(pk=1)
    # print(beer_list.filter(company__name__startswith='H', abv__gte=5))
    # print(beer_list.filter(Q(company__name__startswith='H') | Q(abv__gte=5))) # requiere django.db.models.Q

    #Beer.objects.filter(pk=1).first().delete())

    # print(company.beers) # Estamos llamando al related_name de la ForeignKey company en el modelo Beers

    # for beer in company.beers.all():
    #   print(beer)
    #   beer.abv = 5.4
    #   beer.save()

    # special = SpecialIngredient(name="romero")
    # special.save()
    # print(special)

    # print(Beer.objects.filter(pk__in=[1,2,3,4,5]).first())
    # ingredient = SpecialIngredient.objects.get(pk=1)
    # beer.special_ingredients.add(ingredient)

@login_required
def beer_list_view(request):
 	if request.method == 'GET':
         return render(request, 'beers_list.html', {
             'beer_list': Beer.objects.all()
         })


class BeerListView(LoginRequiredMixin, View):

    def get(self, request):
        return HttpResponse('resultado')


class BeerListViewAlt(AddHappyBirthdayToContextMixin, ListView):
    model = Beer

    def get_queryset(self):
        return Beer.objects.all()
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['company'] = context['beer_list'].first().company
        print(context)
        return context


def beer_detail_view(request, pk):

    # print("user", request.user)
	
    context = {
        'pk': Beer.objects.get(pk=pk)
    }
		
    return render(request, 'beer_detail.html', context)

class BeerDetailView(DetailView):
    model = Beer

    @login_required
    def dispatch(self, request, *args, **kwargs):
        pass


#def company_edit(request, pk):
#    company = get_object_or_404(Company, pk=pk)
#    if request.method == 'POST':
#        print("POST", request.POST)
#        form = CompanyForm(request.POST)
#        if form.is_valid():
#            company.name = form.cleaned_data['name']
#            company.tax_number = form.cleaned_data['tax_number']
#            company.save()
#    else:
#        form = CompanyForm(initial={
#            'name': company.name,
#            'tax_number': company.tax_number,
#        })
#
#    context = {
#        'form': form
#    }
#    
#    return render(request, 'company/company_form.html', context)


class CompanyListView(ListView):
    model = Company

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        self.request.session['counter'] = self.request.session.get('counter', 0) + 1

        ctx['counter'] = self.request.session['counter']
        print(ctx['counter'])

        # Ejemplo de traducción polisémica
        ctx['may_month'] = pgettext('months', 'May')
        ctx['may_person'] = pgettext('person', 'May')
        
        return ctx

def company_edit(request, pk):
    company = get_object_or_404(Company, pk=pk)

    if request.method == 'POST':
        form = CompanyForm(request.POST, instance=company)
        if form.is_valid():
            form.save()
    else:
        form = CompanyForm(instance=company)

    context = {
        'form': form
    }
    
    return render(request, 'company/company_form.html', context)

class CompanyCreateView(CreateView):
    model = Company
    form_class = CompanyForm
    success_url = reverse_lazy('company-detail-view')

class CompanyAndBeersCreateView(CreateView):
    model = Company
    form_class = CompanyForm
    template_name = 'beers/company_create_with_beers.html'
    success_url = reverse_lazy('company-detail-view')

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)

        if self.request.POST:
            ctx['beer_formset'] = BeerFormset(self.request.POST)
        else:
            ctx['beer_formset'] = BeerFormset()
        
        return ctx

    def form_valid(self, form):
        ctx = self.get_context_data()
        beer_formset = ctx['beer_formset']

        if beer_formset.is_valid():
            self.object = form.save()
            beer.formset.instance = self.object
            beer_formset.save()

        return super().form_valid(form)

class CompanyEditView(UpdateView):
    model = Company
    form_class = CompanyForm
    success_url = reverse_lazy('company-detail-view')

class CompanyDetailView(DetailView):
    model = Company