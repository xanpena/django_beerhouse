from rest_framework import viewsets

from beers.api.serializers import BeerSerializer
from beers.models import Beer

class BeerViewSet(viewsets.ModelViewset):
    queryset = Beer.objects.all()
    serializer_class = BeerSerializer


class CompanyViewSet(viewsets.ModelViewset):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer