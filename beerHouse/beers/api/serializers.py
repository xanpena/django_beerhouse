from rest_framework import serializers

from beers.models import Beer

#class BeerSerializer(serializers.Serializer):
#    id = serializers.IntegerField()
#    name = serializers.CharField()

class BeerSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Beer
        fields = ('name', 'abv', 'color', 'is_filtered')

class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'tax_number')