from django.conf.urls import url
from beers.views import beer_list_view, beer_detail_view, BeerListView, BeerListViewAlt, BeerDetailView, CompanyEditView, CompanyDetailView, CompanyListView, CompanyCreateView, CompanyAndBeersCreateView

urlpatterns = [
    url(r'^list/$', beer_list_view, name='beer-list-view'),
    url(r'^list-alt/$', BeerListView.as_view(), name='beer-list-view'),
    url(r'^list-alt-second/$', BeerListViewAlt.as_view(), name='beer-list-view-alt'),
    url(r'^detail/(?P<pk>\d+)$', beer_detail_view, name='beer-detail-view'),
    url(r'^detail-alt/(?P<pk>\d+)$', BeerDetailView.as_view(), name='beer-detail-view-alt'),

    url(r'^company/list/$', CompanyListView.as_view(), name='company-list-view'),
    url(r'^company/create/$', CompanyCreateView.as_view(), name='company-create-view'),
    url(r'^company/create-with-beers/$', CompanyAndBeersCreateView.as_view(), name='company-and-beers-create-view'),
    url(r'^company/edit/(?P<pk>\d+)$', CompanyEditView.as_view(), name='company-edit-view'),
    url(r'^company/detail/(?P<pk>\d+)$', CompanyDetailView.as_view(), name='company-detail-view'),
]