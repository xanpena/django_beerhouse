from django.test import TestCase

# Create your tests here.
class BasicTestClass(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        company = Company.objects.create(name="Hijos de Rivera", tax_number=1)
        Beer.objects.create(name="Estrella Galicia", company=company)
        Beer.objects.create(name="Red Vintage", company=company, abv=5.0)
        Beer.objects.create(name="1906", company=company)

    def test_is_alcoholic(self):
        beer = Beer.objects.get(pk=1)

        self.assertEquals(beer.is_alcoholic, True)

    def test_has_more_alcohol_than(self):
        beer = Beer.objects.get(pk=2)

        self.assertEquals(beer.has_more_alcohol_than(4), True)