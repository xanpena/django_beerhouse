from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from model_mommy import mommy

class CompanyListViewTests(TestCase):

    #@classmethod
    #def setUpTestData(cls):
    #    Company.objects.create(name="company1", tax_number=1)
    #    Company.objects.create(name="company2", tax_number=2)
    #    Company.objects.create(name="company3", tax_number=3)
    #    Company.objects.create(name="company4", tax_number=4)
    #    User.objects.create(username='test_user')

    def setUp(self):
        mommy.make(Company, _quantity=12)
        self.user = mommy.make(User)

    def test_view_url_exists(self):
        url = '/beers/company/list/'
        #self.client.force_login(User.objects.first())
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

    def test_view_returns_all_entries(self):
        url = '/beers/company/list/'
        #self.client.force_login(User.objects.first())
        self.client.force_login(self.user)
        response = self.client.get(url)

        #self.assertEquals(response.context['object_list'].count(), 4)
        self.assertEquals(response.context['object_list'].count(), Company.objects.all().count())

    def test_template(self):
        #self.client.force_login(User.objects.first())
        self.client.force_login(self.user)
        response = self.client.get(reverse('company-list-view'))
        self.assertTemplateUsed(response, 'beers/company_list.html')

    def test_login(self):
        response = self.client.get(reverse('company-list-view'))
        self.assertEquals(response, 302)
