from django.test import TestCase

# Create your tests here.
class BasicTestClass(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        pass

    def setUp(self):
        pass

    def test_true_is_true(self):
        self.assertEquals(True, True)

    def test_suma(self):
        self.assertEquals(4, 2+2)

    def test_error(self):
        self.assertTrue(True, False)