from django.db import models
from beers.utils import image_upload_location
from core.models import CommonInfo
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Company(CommonInfo):
    name = models.CharField('Name', max_length=50)
    tax_number = models.IntegerField('Tax number', unique=True)

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'
        ordering = ['-name']

    def __str__(self):
        return self.name

class Beer(CommonInfo):
    name = models.CharField(_('Name'), max_length=50)
    abv = models.DecimalField(_('Alcohol by volume'), max_digits=5, decimal_places=2, default=0.0)
    is_filtered = models.BooleanField(_('Is filtered?'), default=False)
    COLOR_CHOICES = (
        (1, 'yellow'),
        (2, 'black'),
    )
    color = models.PositiveSmallIntegerField('Color', choices=COLOR_CHOICES, default=1)
    image = models.ImageField('Image', blank=True, null=True, upload_to=image_upload_location)
    company = models.ForeignKey(Company, related_name="beers")

    class Meta:
        verbose_name = 'Beer'
        verbose_name_plural = 'Beers'
        ordering = ['-name']

    def __str__(self):
        return self.name

    @property
    def is_alcoholic(self):
        return self.abv > 0

    def has_more_alcohol_than(self, alcohol):
        return self.abv > alcohol

class SpecialIngredient(CommonInfo):
    name = models.CharField('Name', max_length=50)
    beers = models.ManyToManyField(Beer, blank=True, related_name="special_ingredients")

    class Meta:
        verbose_name = 'Special Ingredient'
        verbose_name_plural = 'Special Ingredients'
        ordering = ['-name']

    def __str__(self):
        return self.name